import { Button, Table } from "antd";
import React from "react";
import { getStorage } from "../../config/storage/storage";

const MyHostelsTable = ({ loading, getHostelDetails }) => {
  const dataSource = getStorage("hostels");
  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Location",
      dataIndex: "location",
      key: "age",
    },
    {
      title: "Actions",
      key: "address",
      render: (record) => {
        return <Button onClick={() => getHostelDetails(record)}>More</Button>;
      },
    },
  ];
  return (
    <div>
      <Table
        loading={loading}
        dataSource={dataSource?.userHostels}
        columns={columns}
      />
    </div>
  );
};

export default MyHostelsTable;
