import { Table } from "antd";
import React from "react";
import { getStorage } from "../../config/storage/storage";

const StudentsTable = () => {
  const details = getStorage("hostel_details");
  const columns = [
    {
      title: "Student",
      render: (text) => text?.student?.name,
      key: "age",
    },

    {
      title: "Room",
      key: "name",
      render: (text) => text?.hostel_room?.name,
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      render: (text) => `${text?.split("-")[0]}`,
    },
    {
      title: "Amount Paid",
      key: "AmmountPaid",
      render: (text) => `${text?.status?.split("-")[1] || "Not Paid"}`,
    },
    {
      title: "End Date",
      dataIndex: "end_date",
      key: "age",
      render: (text) => (text ? new Date(text).toDateString() : "Not Set"),
    },
  ];
  return <Table dataSource={details?.bookings} columns={columns} />;
};

export default StudentsTable;
