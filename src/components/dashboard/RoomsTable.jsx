import { Table, Tag } from "antd";
import React from "react";
import { getStorage } from "../../config/storage/storage";

const RoomsTable = () => {
  const details = getStorage("hostel_details");
  const columns = [
    {
      title: "Room",
      key: "name",
      dataIndex: "name",
    },
    {
      title: "Floor",
      dataIndex: "floor",
      key: "floor",
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Capacity",
      dataIndex: "total_occupants",
      key: "capacity",
    },
    {
      title: "Booked",
      dataIndex: "booked",
      key: "booked",
      render: (text) =>
        text ? <Tag color="success">Yes</Tag> : <Tag color="error">No</Tag>,
    },
  ];
  return (
    <div>
      <Table dataSource={details?.rooms} columns={columns} />
    </div>
  );
};

export default RoomsTable;
