import { Button, message, Modal, Tabs } from "antd";
import React from "react";
import { useState } from "react";
import { addImage, addMyFloors, addMyRooms } from "../../config/api/api";
import { getStorage } from "../../config/storage/storage";
import AddFloor from "./AddFloor";
import AddHostelImage from "./AddHostelImage";
import AddRoom from "./AddRoom";
import AddRoomImage from "./AddRoomImage";
import RoomsTable from "./RoomsTable";
import StudentsTable from "./StudentsTable";

const { TabPane } = Tabs;
const HostelDetails = ({ closeModal, visible }) => {
  const user = getStorage("token");
  const details = getStorage("hostel_details");
  const [addFloorsModal, setAddFloorsModal] = useState(false);
  const [addRoomsModal, setAddRoomsModal] = useState(false);
  const [addHosteImageModal, setAddHostelImageModal] = useState(false);
  const [addRoomImageModal, setAddRoomImageModal] = useState(false);

  const addRoomImage = async (values) => {
    const formData = new FormData();
    formData.append("image", values.image.file.originFileObj);
    formData.append("type", "room");
    message.loading("Loading", 1);
    const roomImage = await addImage(formData, values.id);
    if (roomImage.success) {
      message.success("Successfully added room image");
    } else message.error(roomImage.message);
  };

  const addHostelImage = async (values) => {
    const formData = new FormData();
    formData.append("image", values.image.file.originFileObj);
    formData.append("type", "hostel");
    message.loading("Loading", 1);
    const hostels = await addImage(formData, details.id);
    console.log(hostels);
    if (hostels.success) {
      message.success("Successfully added hostel image");
    } else message.error(hostels.message);
  };

  const openAddRoomImageModal = () => {
    setAddRoomImageModal(true);
  };
  const openAddHostelImageModal = () => {
    setAddHostelImageModal(true);
  };

  const closeAddHostelImageModal = () => {
    setAddHostelImageModal(false);
  };

  const closeAddRoomImageModal = () => {
    setAddRoomImageModal(false);
  };

  const addFloor = async (values) => {
    message.loading("Loading", 1);
    values.hostel_id = details.id;
    const hostels = await addMyFloors(values, user.id);
    if (hostels.success) {
      message.success("Successfully added floor");
    } else message.error(hostels.message);
  };

  const addRoom = async (values) => {
    message.loading("Loading", 1);
    values.occupants = 0;
    const hostels = await addMyRooms(values, user.id);
    if (hostels.success) {
      message.success("Successfully added room");
    } else message.error(hostels.message);
  };

  const closeAddRoom = () => setAddRoomsModal(false);
  const closeAddFloor = () => setAddFloorsModal(false);

  const openAddFloor = () => {
    setAddFloorsModal(true);
  };
  const openAddRoom = (details) => {
    setAddRoomsModal(true);
  };

  return (
    <div>
      <Modal
        title={`${details?.name} Details`}
        width={1000}
        onCancel={closeModal}
        footer={[
          <Button onClick={openAddFloor} type="primary">
            Add Floor
          </Button>,
          <Button onClick={openAddRoom}>Add Room</Button>,
          <Button
            type="dashed"
            className="bg-light"
            onClick={openAddRoomImageModal}
          >
            Add Room Image
          </Button>,
          <Button type="dashed" onClick={openAddHostelImageModal}>
            Add Hostel Image
          </Button>,
        ]}
        open={visible}
      >
        <Tabs>
          <TabPane tab="Rooms" key={"rooms"}>
            <RoomsTable />
          </TabPane>
          <TabPane tab="Student" key={"students"}>
            <StudentsTable />
          </TabPane>
        </Tabs>
        <AddRoom
          add={addRoom}
          visible={addRoomsModal}
          closeModal={closeAddRoom}
        />
        <AddFloor
          add={addFloor}
          visible={addFloorsModal}
          closeModal={closeAddFloor}
        />
        <AddHostelImage
          visible={addHosteImageModal}
          onClose={closeAddHostelImageModal}
          addImage={addHostelImage}
        />
        <AddRoomImage
          visible={addRoomImageModal}
          onClose={closeAddRoomImageModal}
          addImage={addRoomImage}
        />
      </Modal>
    </div>
  );
};

export default HostelDetails;
