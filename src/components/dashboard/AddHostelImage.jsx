import { Button, Form, Modal, Upload } from "antd";
import React from "react";
import { InboxOutlined } from "@ant-design/icons";

const AddHostelImage = ({ visible, onClose, addImage }) => {
  return (
    <Modal
      open={visible}
      onCancel={onClose}
      closable
      footer={null}
      title="Add Hostel Image"
    >
      <Form layout="vertical" onFinish={addImage}>
        <Form.Item
          name="image"
          rules={[{ required: true, message: "Please add an Image" }]}
        >
          <Upload.Dragger name="file" listType={null} maxCount={1}>
            <p className="ant-upload-drag-icon">
              <InboxOutlined />
            </p>
            <p className="ant-upload-text">
              Click or drag Image to this area to upload
            </p>
          </Upload.Dragger>
        </Form.Item>
        <Button type="primary" htmlType="submit">
          Add
        </Button>
      </Form>
    </Modal>
  );
};

export default AddHostelImage;
