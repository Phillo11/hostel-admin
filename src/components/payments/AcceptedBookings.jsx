import { Button, Popover, Space, Table, Tag } from "antd";
import React from "react";
import { getStorage } from "../../config/storage/storage";

const PaymentsTable = ({ loading, setBookingDetails }) => {
  const dataSource = getStorage("accepted_bookings");
  const columns = [
    {
      title: "Room",
      key: "name",
      render: (text) => text?.hostel_room?.name,
    },
    {
      title: "Student",
      render: (text) => text?.student?.name,
      key: "student",
    },
    {
      title: "Paid",
      dataIndex: "paid",
      key: "paid",
      render: (text) =>
        text ? <Tag color="success">Yes</Tag> : <Tag color="error">No</Tag>,
    },
    {
      title: "Actions",
      key: "address",
      render: (record) => {
        return (
          <Popover
            content={
              <Space direction="vertical">
                <Button
                  onClick={() => setBookingDetails(record)}
                  className="bg-light"
                  type="dashed"
                >
                  Accept Payment
                </Button>
              </Space>
            }
            placement="bottom"
          >
            <Button>Actions</Button>
          </Popover>
        );
      },
    },
  ];
  return (
    <div>
      <Table
        loading={loading}
        dataSource={dataSource?.booking}
        columns={columns}
      />
    </div>
  );
};

export default PaymentsTable;
