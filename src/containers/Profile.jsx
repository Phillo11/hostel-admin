import React from "react";
import { AiOutlineUser } from "react-icons/ai";
import MyHostelsTable from "../components/dashboard/MyHostelsTable";
import MainContent from "../components/shared/MainContent";
import { getStorage } from "../config/storage/storage";

const Profile = () => {
  const user = getStorage("token");

  return (
    <>
      <h3 className="fw-light m-3 text-center">User Profile</h3>
      <MainContent>
        <div className="d-flex align-items-center flex-column">
          <h1 className="display-1">
            <AiOutlineUser />
          </h1>
          <h3 className="fw-light">{user?.name}</h3>
          <p>{user.username}</p>
          <h3 className="fw-light">My Hostels</h3>
        </div>
        <MyHostelsTable loading={false} />
      </MainContent>
    </>
  );
};

export default Profile;
