import { Button, Form, message, Select } from "antd";
import React from "react";
import { useState } from "react";
import PaymentsTable from "../components/payments/AcceptedBookings";
import PaymentForm from "../components/payments/Pay";
import MainContent from "../components/shared/MainContent";
import { acceptStudentPayment, getAcceptedBookings } from "../config/api/api";
import { getStorage, setStorage } from "../config/storage/storage";

const Payments = () => {
  const [loading, setLoading] = useState(false);
  const [modal, setModal] = useState(false);
  const [paymentDetails, setPaymentDetails] = useState({});
  const hostels = getStorage("hostels");

  const openModal = (details) => {
    setPaymentDetails(details);
    setModal(true);
  };

  const closeModal = () => {
    setModal(false);
  };

  const acceptPayment = async (values) => {
    message.loading("Loading", 1);
    const accept = await acceptStudentPayment(paymentDetails.id, {
      amount: values?.amount,
    });
    if (accept.success) {
      message.success("Payment successfull");
      setModal(false);
    } else {
      message.error(accept.message);
    }
  };

  const onFinish = async (values) => {
    setLoading(true);
    const bookings = await getAcceptedBookings(values.hostelId);

    if (bookings.success) {
      setStorage("accepted_bookings", bookings);
    } else {
      message.error(bookings.message);
    }
    setLoading(false);
  };
  return (
    <>
      <h3 className="fw-light m-3">Room Payments</h3>
      <MainContent>
        <div className="w-75">
          <Form onFinish={onFinish} layout="horizontal">
            <div className="d-flex">
              <Form.Item
                rules={[{ required: true, message: "Please select a hostel" }]}
                className="w-75 mx-2"
                name={"hostelId"}
              >
                <Select placeholder="Select Hostel">
                  {hostels?.userHostels?.map((hostel) => (
                    <Select.Option value={hostel.id} key={hostel.id}>
                      {hostel.name}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Button loading={loading} type="primary" htmlType="submit">
                Search
              </Button>
            </div>
          </Form>
        </div>
        <PaymentsTable loading={loading} setBookingDetails={openModal} />
        <PaymentForm
          acceptPayment={acceptPayment}
          visible={modal}
          closeModal={closeModal}
        />
      </MainContent>
    </>
  );
};

export default Payments;
