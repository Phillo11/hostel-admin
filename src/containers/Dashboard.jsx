import { Button, message, Popover, Space } from "antd";
import React, { useEffect, useState } from "react";
import AddHostel from "../components/dashboard/AddHostel";
import HostelDetails from "../components/dashboard/HostelDetails";
import MyHostelsTable from "../components/dashboard/MyHostelsTable";
import MainContent from "../components/shared/MainContent";
import { addMyHostel, getHostels, getMyHostelDetails } from "../config/api/api";
import { getStorage, setStorage } from "../config/storage/storage";

const Dashboard = () => {
  const user = getStorage("token");
  const [loading, setLoading] = useState(false);
  const [modal, setModal] = useState(false);
  const [addModal, setAddModal] = useState(false);

  const getMyHostels = async () => {
    setLoading(true);
    const hostels = await getHostels(user?.id);
    if (hostels.success) setStorage("hostels", hostels);
    else message.error(hostels.message);
    setLoading(false);
  };

  const closeDetailsModal = () => setModal(false);

  const getHostelDetails = async (record) => {
    message.loading("Loading", 1);
    const hostels = await getMyHostelDetails(record?.id);
    if (hostels.success) setStorage("hostel_details", hostels);
    else message.error(hostels.message);
    setModal(true);
  };

  const openAddModal = () => {
    setAddModal(true);
  };

  const closeAddModal = () => {
    setAddModal(false);
  };

  const addHostel = async (values) => {
    message.loading("Loading", 1);
    const hostels = await addMyHostel(values, user.id);
    if (hostels.success) {
      message.success("Successfully added modal");
      getMyHostels();
    } else message.error(hostels.message);
    setAddModal(false);
  };

  useEffect(() => {
    getMyHostels();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <>
      <h3 className="fw-light m-3">My Hostels</h3>
      <MainContent>
        <Space className="my-3">
          <Popover
            placement="bottom"
            content={
              <Space direction="vertical">
                <Button onClick={openAddModal} type="primary">
                  Add Hostel
                </Button>
                <Button className="w-100" onClick={getMyHostels}>
                  Refresh
                </Button>
              </Space>
            }
          >
            <Button type="primary">Actions</Button>
          </Popover>
        </Space>
        <MyHostelsTable getHostelDetails={getHostelDetails} loading={loading} />
        <HostelDetails visible={modal} closeModal={closeDetailsModal} />
        <AddHostel
          closeModal={closeAddModal}
          visible={addModal}
          addHostel={addHostel}
        />
      </MainContent>
    </>
  );
};

export default Dashboard;
