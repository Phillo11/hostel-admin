import { Button, Checkbox, Form, Input, message } from "antd";
import React, { useState } from "react";
import { AiOutlineBank } from "react-icons/ai";
import { useNavigate } from "react-router-dom";
import { loginUser } from "../config/api/api";
import { setStorage } from "../config/storage/storage";

const Login = () => {
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const onFinish = async (values) => {
    setLoading(true);
    const data = await loginUser(values);
    if (data.success) {
      if (data.user.type === "Student") {
        message.error("Admin Portal Only");
      } else {
        message.success("Login Successful");
        setStorage("token", data.user);
        navigate("/");
      }
    } else {
      message.error(data.message);
    }
    setLoading(false);
  };

  const onFinishFailed = async (errorInfo) => {};

  return (
    <div className="w-100 bg-white">
      <div className="d-flex align-items-center m-5 flex-column">
        <h1 className="display-1">
          <AiOutlineBank />
        </h1>

        <h1 className="fw-light">E Hostel Website</h1>
        <h4 className="fw-light">Login</h4>
      </div>
      <div className="d-flex justify-content-center">
        <div className="w-25">
          <Form
            name="basic"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Username"
              name="username"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item name="remember" valuePropName="checked">
              <Checkbox>Remember me</Checkbox>
            </Form.Item>

            <Form.Item>
              <Button loading={loading} type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default Login;
